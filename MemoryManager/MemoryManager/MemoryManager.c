
//MemoryManager.c by Silas Baker(0950941) & Laura Reilly (0972157)
//We certify that this work is
//entirely our own.The assessor of this project may reproduce this project
//and provide copies to other academic staff, and/or communicate a copy of
//this project to a plagiarism - checking service, which may retain a copy of the
//project on its database.



//#include <MemoryManager.h>

#include <stdlib.h>
#include <stdbool.h>

//Type Deffinitions
//typedef enum { false, true } bool;
typedef struct MemoryBlock MemoryBlock;
typedef struct MemoryAllocator MemoryAllocator;
typedef unsigned char byte;

struct MemoryBlock
{
	MemoryBlock *pNext,
				*pPrevious;
	size_t allocationSize;
	//void* allocation;
	bool isFree;
};

#define BLOCK_SIZE sizeof(MemoryBlock)

struct MemoryAllocator
{
	byte* memoryPool;
	size_t totalPoolSize;
	size_t freePoolSize;
	MemoryBlock* memoryBlock;
};

void setupBlock(MemoryBlock* block)
{
	block->allocationSize = 0;
	block->pNext = NULL;
	block->pPrevious = NULL;
}

//Returns true if memory has been allocated succesfully
bool setupAllocationPool(size_t poolSize, MemoryAllocator* memAllocator)
{
	//if (memAllocator->memoryPool) //How do we validate this
	//{
		memAllocator->totalPoolSize = memAllocator->freePoolSize = poolSize;
		memAllocator->memoryPool = (byte*) malloc(poolSize);
		memAllocator->memoryBlock = (MemoryBlock*)memAllocator->memoryPool;

		for (size_t i = 0; i < poolSize; i++)
		{
			memAllocator->memoryPool[i] = 'c';
		}
		memAllocator->memoryBlock->allocationSize = poolSize - BLOCK_SIZE;
		memAllocator->memoryBlock->pNext = NULL;
		memAllocator->memoryBlock->pPrevious = NULL;

		memAllocator->memoryPool[poolSize -1] = 'X';
		return true;
	//}
	//
	//return false; //The memory has already been allocated
}

void cleanUpAllocationPool(MemoryAllocator* memAllocator)
{
	free(memAllocator->memoryPool);
	memAllocator->totalPoolSize = memAllocator->freePoolSize = 0;
}

//Custom malloc
void* cMalloc(size_t size, MemoryAllocator* allocator)
{
	MemoryBlock* currentBlock = allocator->memoryBlock;

	if (!currentBlock || allocator->freePoolSize < size + BLOCK_SIZE) return 0; //allocator isnt setup correctly

	//Find empty space that is big enough
	while (currentBlock != 0)
	{
		if (currentBlock->isFree && currentBlock->allocationSize >= size + BLOCK_SIZE)
		{
			//Found a space!
			break;
		}

		currentBlock = currentBlock->pNext;
	}

	if (currentBlock->allocationSize < size + BLOCK_SIZE) return 0; //There is no avalable space

	void* nBlock = currentBlock + 1;//BLOCK_SIZE + size;
	MemoryBlock* newBlock = (MemoryBlock*)((char*)nBlock + size);//(MemoryBlock*)nBlock;//(MemoryBlock*)(((byte*)currentBlock) + BLOCK_SIZE + size);
	setupBlock(newBlock);

	newBlock->isFree = true;
	newBlock->pNext = currentBlock->pNext;
	newBlock->pPrevious = currentBlock;
	if (currentBlock->pNext) 
		currentBlock->pNext->pPrevious = newBlock;
	currentBlock->pNext = newBlock;

	newBlock->allocationSize = currentBlock->allocationSize - BLOCK_SIZE - size;
	currentBlock->isFree = false;
	currentBlock->allocationSize = size;
	allocator->freePoolSize -= size + BLOCK_SIZE;

	return currentBlock + 1;
}

//Custom free
void cFree(void* allocation, MemoryAllocator* allocator)
{
	//if (allocation < allocator->memoryPool || allocation > allocator->memoryPool + allocator->totalPoolSize) return; //outside of the allocator

	MemoryBlock* currentBlock = (MemoryBlock*)((byte*)allocation - BLOCK_SIZE);

	//There is a not free block to the left and the right 
	if (currentBlock->pPrevious != 0 && currentBlock->pNext != 0 && !currentBlock->pPrevious->isFree && !currentBlock->pNext->isFree)
	{
		currentBlock->isFree = true;
		allocator->freePoolSize += currentBlock->allocationSize;
	}
	
	if (currentBlock->pNext != 0 && currentBlock->pNext->isFree) //If there is an empty block after this one, merge
	{
		size_t nextBlockSize = currentBlock->pNext->allocationSize + BLOCK_SIZE;

		currentBlock->pNext = currentBlock->pNext->pNext;

		if (currentBlock->pNext != 0)
		{
			currentBlock->pNext->pPrevious = currentBlock;
		}

		currentBlock->isFree = true;
		currentBlock->allocationSize = currentBlock->allocationSize + nextBlockSize;
		allocator->freePoolSize += currentBlock->allocationSize;
	}

	if (currentBlock->pPrevious != 0 && currentBlock->pPrevious->isFree) //If there is an empty block after this one, merge
	{
		size_t memBlockSize = currentBlock->allocationSize + BLOCK_SIZE;

		currentBlock = currentBlock->pPrevious;

		currentBlock->pNext = currentBlock->pNext->pNext;
		if (currentBlock->pNext != 0)
			currentBlock->pNext->pPrevious = currentBlock;

		currentBlock->allocationSize = currentBlock->allocationSize + memBlockSize;
		allocator->freePoolSize += currentBlock->allocationSize;
	}

	allocation = 0;
}

char* visualizeMemory(MemoryAllocator* allocator, char* blockString)
{
	MemoryBlock* temp = allocator->memoryBlock;
	unsigned int i;
	unsigned int displayWidth = 100;
	size_t poolWidth = allocator->totalPoolSize;
	printf("\n\n");

	for (i = 0; i < displayWidth; ++i)
	{
		//blockString[i] = '=';
		printf("=");
	}
	//blockString[displayWidth] = '\n';
	printf("\n");

	size_t currentSize = 0;
	while (temp != 0)
	{		
		float positionPercentage = (float)(currentSize) / poolWidth;
		size_t headerPos = (size_t)(positionPercentage * 100);
		//blockString[displayWidth + 2 + headerPos] = '|';
		

		float allocationPercentage = (float)temp->allocationSize / poolWidth;
		unsigned int allocationWidth = (unsigned int)(allocationPercentage * 100);
		printf("|");
		for (i = 0; i <= allocationWidth; ++i)
		{
			if (!temp->isFree)
			{
				//blockString[i] = '~';
				printf("~");
			}
			else
			{
				//blockString[i] = ' ';
				printf(" ");
			}
		}

		currentSize += temp->allocationSize + BLOCK_SIZE;
		temp = temp->pNext;
	}
	printf("\n");
	//blockString[2*displayWidth + 1] = '\n';
	for (i = 0; i < displayWidth; ++i)
	{
		//blockString[i] = '=';
		printf("=");
	}
	printf("\n");

	//blockString[3 * displayWidth + 3] = 0;

	return blockString;
}