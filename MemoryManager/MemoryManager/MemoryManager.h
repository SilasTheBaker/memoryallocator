////#ifndef _MEMORY_MANAGER_H_
////#define _MEMORY_MANAGER_H_
//
//#pragma once
//
////We certify that this work is
////entirely our own.The assessor of this project may reproduce this project
////and provide copies to other academic staff, and/or communicate a copy of
////this project to a plagiarism - checking service, which may retain a copy of the
////project on its database.
//
////Modified By Silas Baker
//
//#ifdef __cplusplus
//extern "C" {
//#endif
//
//#include <stdlib.h>
//
////Type Deffinitions
//typedef enum { false, true } bool;
//typedef struct MemoryBlock MemoryBlock;
//typedef struct MemoryAllocator MemoryAllocator;
//typedef unsigned char byte;
//
//struct MemoryBlock
//{
//	MemoryBlock *pNext,
//		*pPrevious;
//	size_t allocationSize;
//	void* allocation;
//	bool isFree;
//};
//
//#define BLOCK_SIZE = sizeof(MemoryBlock);
//
//struct MemoryAllocator
//{
//	byte* memoryPool;
//	size_t totalPoolSize;
//	size_t freePoolSize;
//};
//
//#ifdef __cplusplus
//}
//#endif
//
////#endif // _MEMORY_MANAGER_H_