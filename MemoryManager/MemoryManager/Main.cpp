//Main.cpp by Silas Baker(0950941) & Laura Reilly (0972157)
//We certify that this work is
//entirely our own.The assessor of this project may reproduce this project
//and provide copies to other academic staff, and/or communicate a copy of
//this project to a plagiarism - checking service, which may retain a copy of the
//project on its database.

//Modified By Silas Baker
#include <stdio.h>
#include <iostream>
#include "MemoryManager.c"

int main()
{
	MemoryAllocator memAllocator;

	if (setupAllocationPool(10000, &memAllocator))
	{
		char blockString[303]; //300 chars + newline and null terminator

		char* test = (char*)cMalloc(sizeof(char) * 50, &memAllocator);
		int* test1 = (int*)cMalloc(sizeof(int) * 50, &memAllocator);
		float* test2 = (float*)cMalloc(sizeof(float) * 50, &memAllocator);
		MemoryBlock* test3 = (MemoryBlock*)cMalloc(BLOCK_SIZE * 50, &memAllocator);
		MemoryBlock* test4 = (MemoryBlock*)cMalloc(BLOCK_SIZE * 100, &memAllocator);

		//printf("\n\n\n%s", visualizeMemory(&memAllocator, blockString));
		visualizeMemory(&memAllocator, blockString);

		cFree(test2, &memAllocator);
		visualizeMemory(&memAllocator, blockString);
		cFree(test1, &memAllocator);
		visualizeMemory(&memAllocator, blockString);


		for (int i = 0; i < 20; i++)
		{
			test[i] = 'L';//(char)((i % 26) + 101);
			//test1[i] = i;
			test2[i] = 'S';//i + 0.7623f * i;
		}
		/*
		int breakpoint = 0;

		std::cout << "Allocation size: " << memAllocator.totalPoolSize << " bytes, using: " << memAllocator.totalPoolSize  - memAllocator.freePoolSize << " bytes" << std::endl;

		std::cout << "\n\nAllocation " << "1" << ": " << std::endl;
		for (int i = 0; i < 10; i++)
		{
			std::cout << test[i] << " ";
		}

		std::cout << "\n\nAllocation " << "2" << ": " << std::endl;
		for (int i = 0; i < 10; i++)
		{
			std::cout << test1[i] << " ";
		}

		std::cout << "\n\nAllocation " << "3" << ": " << std::endl;
		for (int i = 0; i < 10; i++)
		{
			std::cout << test2[i] << " ";
		}*/
	}
	cleanUpAllocationPool(&memAllocator);
	std::cin.get();
	return 0;
}